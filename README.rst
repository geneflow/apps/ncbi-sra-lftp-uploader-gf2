NCBI SRA LFTP Uploader App
==========================

Version: 0.1.4

This GeneFlow2 app wraps LFTP for NCBI SRA data upload

Inputs
------

1. reads: Directory where read or genome file is located

Parameters
----------

1. ftp_site: NCBI ftp link

2. user: User name for ftp submission

3. password: Password for ftp submission

4. remote: directory for submission

5. new_output_dir: Name of remote directory to upload files

6. output: Output directory name for logs

7. delete: set to 'delete' to delete any remote files that are not in the local folder being uploaded
